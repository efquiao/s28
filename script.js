// //Getting all todos
fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then((response) => response.json())
  .then((data) =>
    console.log(
      `The item ${data.id} "${data.title}" has a status of ${data.completed}.`
    )
  );

//Getting a specific todo
// fetch("https://jsonplaceholder.typicode.com/todos")
//   .then((response) => response.json())
//   .then((data) => {
//     let listOfTodos = data.map((todo) => {
//       return `TITLE#${todo.id}: ${todo.title}`;
//     });
//     console.log(listOfTodos);
//   });

//Create a to do list item using POST Method

fetch("https://jsonplaceholder.typicode.com/todos", {
  method: "POST",
  headers: {
    "Content-type": "application/json",
  },
  body: JSON.stringify({
    title: "Created to do List Item",
    completed: false,
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((data) => console.log(data));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  headers: {
    "Content-type": "application/json",
  },
  body: JSON.stringify({
    title: "Updated  to do List Item",
    description: "To update the my to do list with a different data structure",
    status: "Pending",
    dateCompleted: "Pending",
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((data) => console.log(data));

//PATCH

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PATCH",
  headers: {
    "Content-type": "application/json",
  },
  body: JSON.stringify({
    status: "Complete",
    dateCompleted: "01/19/22",
  }),
})
  .then((response) => response.json())
  .then((data) => console.log(data));

//DELETE
  fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "DELETE",
})
  .then((response) => response.json())
  .then((data) => console.log(data));